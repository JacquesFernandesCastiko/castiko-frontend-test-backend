import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';

import { StudentType } from './StudentTypeQL.js';
import { Student } from './StudentSchema.js';

var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {
  getStudents: {
    name: "getStudents",
    type: new GraphQLList(StudentType),
    args: {
      name: {type: GraphQLString},
      sex: {type: GraphQLString},
      age: {type: GraphQLInt},
      height: {type: GraphQLInt},
      grade: {type: GraphQLString},
      phone_number: {type: GraphQLString},
    },
    resolve: (source, args) => {
      return new Promise((resolve, reject) => {
        Student.find(args, (err, students) => {
          if(err){
            console.log(err);
            reject(err);
          }
          else{
            resolve(students);
          }
        });
      });
    }
  }
}