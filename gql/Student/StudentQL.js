module.exports = {
  StudentQueries: require('./StudentQueriesQL.js'),
  StudentMutations: require('./StudentMutationsQL.js')
};