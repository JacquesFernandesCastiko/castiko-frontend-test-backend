import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
} from 'graphql';

var StudentType = new GraphQLObjectType({
  name: "StudentType",
  fields: () => ({
    name: {type: GraphQLString},
    sex: {type: GraphQLString},
    age: {type: GraphQLInt},
    height: {type: GraphQLInt},
    grade: {type: GraphQLString},
    phone_number: {type: GraphQLString},
  })
});

module.exports = {
  StudentType: StudentType,
}