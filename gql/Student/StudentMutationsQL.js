import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLNonNull,
} from 'graphql';

import { StudentType } from './StudentTypeQL.js';
import { Student } from './StudentSchema.js';

var _ = require('lodash');
var Promise = require('bluebird');

module.exports = {
  addStudent: {
    name:"addStudent",
    type: StudentType,
    args: {
      name: {type: GraphQLString},
      sex: {type: GraphQLString},
      age: {type: GraphQLInt},
      height: {type: GraphQLInt},
      grade: {type: GraphQLString},
      phone_number: {type: new GraphQLNonNull(GraphQLString)},
    },
    resolve: (source, args) => {
      return new Promise((resolve, reject) => {
        var new_student = new Student(args);
        
        new_student.save((err) => {
          if (err){
            console.log(err);
            reject(err);
          }
          else{
            resolve(new_student);
          }
        })
      });
    }
  }
};