import {

} from 'graphql';

var mongoose = require('mongoose');

var StudentSchema = new mongoose.Schema({
  name: {type: String},
  sex: {type: String},
  age: {type: Number},
  height: {type: Number},
  grade: {type: String},
  phone_number: {type: String},
});

var Student = mongoose.model('Student',StudentSchema);

module.exports = {
  Student: Student
}