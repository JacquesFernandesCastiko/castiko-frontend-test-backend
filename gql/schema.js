import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema
} from "graphql";

import {
  StudentMutations,
  StudentQueries
} from './Student/StudentQL.js';

var rootQuery = new GraphQLObjectType({
  name: "Query",
  fields: () => ({
    getStudents: StudentQueries.getStudents,
  })
});

var rootMutation = new GraphQLObjectType({
  name: "Mutation",
  fields: () => ({
    addStudent: StudentMutations.addStudent,
  })
});

module.exports.rootSchema = new GraphQLSchema({
  query: rootQuery,
  mutation: rootMutation
})