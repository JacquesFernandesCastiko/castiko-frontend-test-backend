# Castiko Back-end for the front-end dev test

# Starting the Back-end server:
 - ```git clone``` this repo
 - ```cd castiko-test-backend-gql```
 - ```npm install```
 - start mongodb (depends on your OS)
 - ```npm start```